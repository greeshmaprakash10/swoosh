package com.unitybees.swoosh

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.unitybees.swoosh.Model.Player
import com.unitybees.swoosh.Utilities.EXTRA_PLAYER
import kotlinx.android.synthetic.main.activity_beginers.*

class BeginersActivity : AppCompatActivity()
{

    lateinit var player: Player

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_beginers)

        player=intent.getParcelableExtra(EXTRA_PLAYER)

        beginBtn.setOnClickListener {
            ballBtn.isClickable=false
            player.skill="Beginner"
        }
        ballBtn.setOnClickListener {
            beginBtn.isClickable=false
            player.skill="Baller"
        }

        finishBtn.setOnClickListener {
            if(player.skill!="")
            {
                var beginIntent=Intent(this,FinishActivity::class.java)
                beginIntent.putExtra(EXTRA_PLAYER,player)
                startActivity(beginIntent)
            }
        }


    }
}
