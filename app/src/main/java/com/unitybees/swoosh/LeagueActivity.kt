package com.unitybees.swoosh

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.unitybees.swoosh.Model.Player
import com.unitybees.swoosh.Utilities.EXTRA_PLAYER
import kotlinx.android.synthetic.main.activity_league.*

class LeagueActivity : AppCompatActivity() {

    var player=Player("","")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_league)

        mens.setOnClickListener {
            womens.isChecked=false
            co_ed.isChecked=false

            player.league="mens"
        }
        womens.setOnClickListener {
            mens.isChecked=false
            co_ed.isChecked=false

            player.league="womens"
        }
        co_ed.setOnClickListener {
            mens.isChecked=false
            womens.isChecked=false

            player.league="co-ed"
        }

        nextBtn.setOnClickListener {
            if(player.league!="")
            {
                val nextIntent=Intent(this,BeginersActivity::class.java)
                nextIntent.putExtra(EXTRA_PLAYER,player)
                startActivity(nextIntent)
            }
            else
            {
                Toast.makeText(this,"Please select a League",Toast.LENGTH_SHORT).show()
            }

        }
    }
}
