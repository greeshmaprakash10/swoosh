package com.unitybees.swoosh

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.unitybees.swoosh.Model.Player
import com.unitybees.swoosh.Utilities.EXTRA_PLAYER
import kotlinx.android.synthetic.main.activity_finish.*

class FinishActivity : AppCompatActivity() {

    lateinit var player: Player
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_finish)
        player=intent.getParcelableExtra(EXTRA_PLAYER)

        searchTxt.text="Looking for a ${player.league} ${player.skill} league near you..."


    }
}

